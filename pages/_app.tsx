import '../styles/globals.css'
import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return <div>
    navbar
    <input type="text"/>

    <hr/>
    <Component {...pageProps} />
  </div>
}

export default MyApp
